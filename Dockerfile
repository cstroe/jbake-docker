FROM adoptopenjdk/openjdk8:jdk8u172-b11

RUN apt update && apt install unzip && rm -fr /var/lib/apt/lists

RUN curl -L -H "Accept: application/zip" \
  https://dl.bintray.com/jbake/binary/jbake-2.6.1-bin.zip \
  --output /tmp/jbake.zip && \
  unzip /tmp/jbake.zip -d /opt && \
  mv /opt/jbake-2.6.1-bin /opt/jbake && \
  rm -f /tmp/jbake.zip /opt/jbake/*.zip /opt/jbake/bin/jbake.bat

ENV PATH="/opt/jbake/bin:${PATH}"

# Create a few users to own our files
RUN for i in $(seq 1000 1100); do useradd -u $i jbake-$i; done

ADD /entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
