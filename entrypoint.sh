#!/usr/bin/env bash

if [ -n "$DEBUG" ]; then
  set -x
fi

USER="jbake-1000"

if [ -n "$UID" ]; then
  if [ "$UID" -lt "1000" ]; then
    echo "UIDs less than 1000 are not allowed."
    exit 1
  fi

  if [ "$UID" -le "1100" ]; then
    USER="jbake-$UID"
  else
    useradd -u $UID jbake-$UID
    USER="jbake-$UID"
  fi
fi

su $USER -c "/opt/jbake/bin/jbake $*"
