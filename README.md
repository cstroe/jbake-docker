# jbake-docker

A [Docker image](https://hub.docker.com/r/cstroe/jbake/) to run the [JBake](https://github.com/jbake-org/jbake) static site generator.  See the JBake documentation on the [jbake.org](https://jbake.org/) website.

## Usage

Assuming your JBake files are in the `src/jbake` directory in your project, you can run the following from the root directory of your JBake project:

    docker run --rm -it \
      -e UID=$(id -u) \
      -e DEBUG=true \
      -v $(pwd):/project \
      cstroe/jbake:2.6.1 /project/src/jbake /project/target/output


This will mount your current project directory into the Docker container and generate the static site, owned by yourself.

**NOTE:** This image accepts a `UID` environment variable.  This is a potential security risk if misused.  Don't let untrusted users run `docker` commands.

## Environment Variables

* `UID` - The UID of the owner of the generated files.  Usually your current user's UID.
* `DEBUG` - If non-empty, will enable bash script debug mode for the `entrypoint.sh` script.

## Contributing

* Source repository: [gitlab.com/cstroe/jbake-docker](https://gitlab.com/cstroe/jbake-docker)
* [Issues](https://gitlab.com/cstroe/jbake-docker/issues)

## Links

* [DockerHub page](https://hub.docker.com/r/cstroe/jbake/)
